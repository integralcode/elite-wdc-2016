const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');
const csslint = require('gulp-csslint');
const watch = require('gulp-watch');

gulp.task('copy-css-vendor', function() {
    gulp.src([
            './node_modules/purecss/build/pure-min.css',
            './node_modules/purecss/build/grids-responsive-min.css',
            './node_modules/purecss/build/grids-responsive-old-ie-min.css',
        ])
        .pipe(gulp.dest('./assets/css/vendor'));
});

gulp.task('copy-js-vendor', function() {
    gulp.src([
            './src/js/vendor/*.js',
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/jquery-smooth-scroll/jquery.smooth-scroll.min.js',
        ])
        .pipe(gulp.dest('./assets/js/vendor'));
});

gulp.task('lint-css', function() {
    gulp.src('./src/css/*.css')
        .pipe(csslint())
        .pipe(csslint.reporter());
});

gulp.task('minify-css', ['lint-css'], function() {
    return gulp.src('./src/css/*.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./src/build/css'));
});

gulp.task('styles', ['minify-css'], function() {
    return gulp.src('./src/build/css/*.css') // watch for file order! careful!
        .pipe(concat('app.css'))
        .pipe(gulp.dest('./assets/css/'));
});

gulp.task('lint-js', function() {
    return gulp.src('./src/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('compress-js', ['lint-js'], function() {
    return gulp.src('./src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./src/build/js/'));
});

gulp.task('scripts', ['compress-js'], function() {
    return gulp.src(['./src/build/js/*.js']) // watch for file order! careful!
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./assets/js/'));
});

gulp.task('default', [
        'copy-css-vendor',
        'copy-js-vendor',
        'styles',
        'scripts'
    ],
    function() {
        gulp.watch([
            './src/**/*.css',
            './src/**/*.js'
        ], [
            'copy-css-vendor',
            'copy-js-vendor',
            'styles',
            'scripts'
        ]);
    }
);
